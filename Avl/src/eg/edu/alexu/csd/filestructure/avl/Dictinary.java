package eg.edu.alexu.csd.filestructure.avl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.JFileChooser;

public class Dictinary  implements IDictionary {

    AvlTree<String> dict;

    public Dictinary() {
        dict = new AvlTree<>();
    }
    public void Load() {

        JFileChooser fc = new JFileChooser();
        fc.addChoosableFileFilter(new txtSaveFilter());
        int sf = fc.showSaveDialog(fc);
        if (sf == JFileChooser.APPROVE_OPTION)
            try {
                BufferedReader in = new BufferedReader(new FileReader(fc.getSelectedFile()));
                String str;
                while ((str = in.readLine()) != null)
                    dict.insert(str);
                in.close();
            } catch (IOException e) {
            }
    }

    @Override
    public void load(File file) {
        try {
            BufferedReader in = new BufferedReader(new FileReader(file));
            String str;
            while ((str = in.readLine()) != null)
                dict.insert(str);
            in.close();
        } catch (IOException e) {
        }
        
    }
    @Override
    public boolean insert(String word) {
        Boolean ex =dict.search(word);
        if(!ex)
            dict.insert(word);
        return !ex;
    }
    @Override
    public boolean exists(String word) {
        
        return dict.search(word);
    }
    @Override
    public int size() {
        
        return dict.getSize();
    }
    @Override
    public int height() {
    
        return dict.getSize();
    }
    @Override
    public boolean delete(String word) {
        
        return dict.delete(word);
    }
}
