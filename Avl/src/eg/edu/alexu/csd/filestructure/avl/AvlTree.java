package eg.edu.alexu.csd.filestructure.avl;

import java.util.Scanner;

public class AvlTree <T extends Comparable<T>> implements IAVLTree<T>{

    private int size;
    private Node<T> root;

    public AvlTree() {
        size = 0;
    }
    @Override
    public void insert(T key) {
        root = insert(root, key);
        BTreePrinter.printNode(root);
    }

    private Node<T> insert(Node<T> n, T key) {
        // System.out.println(key.compareTo(n.value));
        if (n == null) {
            n = new Node<T>(key);
            size++;
        } else if (key.compareTo(n.value) > 0)
            n.right = insert(n.right, key);
        else if (key.compareTo(n.value) < 0)
            n.left = insert(n.left, key);
        
        if (Math.abs(getPalance(n)) > 1)
            n = palance(n);
        n.height = 1 + Math.max(getHeight(n.left), getHeight(n.right));
        // System.out.println(key.compareTo(n.value));
       
        return n;
    }
    @Override
    public boolean delete(T key) {
        boolean exist =search(key);
        root = delete(root, key);
        BTreePrinter.printNode(root);
        return exist;
    }

    private Node<T> delete(Node<T> n, T key) {
        if (key == null || n == null) {
            return n;
        } else if (key.compareTo(n.value) > 0)
            n.right = delete(n.right, key);
        else if (key.compareTo(n.value) < 0) {
            n.left = delete(n.left, key);
        } else {
            if (n.left == null && n.right == null) { // no child
                size--;
                return  n= null;
            } else {
                if (n.left == null) {
                    n = n.right;
                } else if (n.right == null)
                    n = n.left;
                else { // two children
                    size++; // in this case the delete will be called twice
                    Node<T> temp = getSuccesor(n);
                    n.value = temp.value;
                    n.right=delete(n.right, n.value);
                }
                size--;
            }
        }
        if (Math.abs(getPalance(n)) > 1)
            n = palance(n);
        n.height = 1 + Math.max(getHeight(n.left), getHeight(n.right));

        // System.out.println(key.compareTo(n.value));
        return n;
    }

    private Node<T> getSuccesor(Node<T> n) {
        Node<T> temp = n.right;
        while (temp.left != null)
            temp = temp.left;
        return temp;
    }
    private int getPalance(Node<T> n) {
        return n.palance = getHeight(n.left) - getHeight(n.right);
    }
    private Node<T> palance(Node<T> n) {
        if (getPalance(n) > 1)
            if (getPalance(n.left) >= 0)
                n = rightRotate(n);
            else {
                n.left = leftRotate(n.left);
                n = rightRotate(n);
            }
        else if (getPalance(n) < -1) {
            if (getPalance(n.right) <= 0)
                n = leftRotate(n);
            else {
                n.right = rightRotate(n.right);
                n = leftRotate(n);
            }

        }
        n.palance = getHeight(n.left) - getHeight(n.right);
        return n;
    }

    @Override
    public boolean search(T key) {
        return search(root, key);
    }

    private boolean search(Node<T> n, T key) {
        if (key == null || n == null) {
            return false;
        } else if (key.compareTo(n.value) > 0)
            return search(n.right, key);
        else if (key.compareTo(n.value) < 0)
            return search(n.left, key);
        else
            return true;
    }

    @Override
    public int height() {
        return getHeight(root);
    }

    public int getSize() {
        return size;
    }

    private int getHeight(Node<T> n) {
        if (n == null) {
            return 0;
        }
        return n.height;
    }

    private Node<T> leftRotate(Node<T> n) {
        Node<T> temp1, temp2;
        temp1 = n.right;
        temp2 = temp1.left;
        temp1.left = n;
        n.right = temp2;
        temp1.height = 1 + Math.max(getHeight(temp1.left), getHeight(temp1.right));
        n.height = 1 + Math.max(getHeight(n.left), getHeight(n.right));
        return temp1;
    }

    private Node<T> rightRotate(Node<T> n) {
        Node<T> temp1, temp2;
        temp1 = n.left;
        temp2 = temp1.right;
        temp1.right = n;
        n.left = temp2;
        temp1.height = 1 + Math.max(getHeight(temp1.left), getHeight(temp1.right));
        n.height = 1 + Math.max(getHeight(n.left), getHeight(n.right));
        return temp1;
    }

   
    @Override
    public INode<T> getTree() {
        return root;
    }
    public static void main(String[] args) {
        AvlTree<Integer> avl = new AvlTree<>();
        Scanner in = new Scanner(System.in);
        for (int i = 0; i < 10; i++) {
            avl.insert(in.nextInt());
        }
        for (int i = 0; i < 1; i++) {
            System.out.println(avl.search(in.nextInt()));
        }
        for (int i = 0; i < 5; i++) {
            avl.delete(in.nextInt());
            
        }

    }
}
