package eg.edu.alexu.csd.filestructure.avl;

public class Node<T extends Comparable<T>> implements INode<T> {

    public Node<T> left, right;
    T value;
    int palance, height;

    public Node() {
        palance = 0;
        height = 1;
    }

    public Node(T value) {
        palance = 0;
        height = 1;
        this.value = value;
    }

    @Override
    public INode<T> getLeftChild() {
        return left;
    }

    @Override
    public INode<T> getRightChild() {
        return right;
    }

    @Override
    public T getValue() {

        return value;
    }

    @Override
    public void setValue(T value) {
        this.value = value;

    }

}
