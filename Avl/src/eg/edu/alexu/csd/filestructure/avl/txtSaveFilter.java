package eg.edu.alexu.csd.filestructure.avl;
import java.io.File;

import javax.swing.filechooser.FileFilter;
/**
 * filter for txt files
 * @author said
 *
 */
public class txtSaveFilter extends FileFilter{

    @Override
    public boolean accept(File f) {
        // TODO Auto-generated method stub
        if(f.isDirectory()){
            return false ;
        }
        String s = f.getName();
        return s.endsWith(".TXT")||s.endsWith(".txt");
        
    }
    
    @Override
    public String getDescription() {
        // TODO Auto-generated method stub
        return ".txt,.TXT";
    }

}
